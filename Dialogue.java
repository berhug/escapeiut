import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public abstract class Dialogue extends Game{

    public static void loadText(String path) {
        String text = path;
        System.out.println(Read.readLineByLine(text));
    }
}
