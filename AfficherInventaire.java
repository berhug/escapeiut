import java.lang.Enum;

public enum AfficherInventaire {

  Afficher ("Voici les objets actuellement present dans votre inventaire :"),
  Vetements ("Vetements : sans quoi, le jeu serait interdit aux moins de 18 ans."),
  Crayon ("Crayon de papier : Se battre a mains nues ? Boring ! Avec un crayon de papier, c'est tout de suite plus amusant.");

  private String inventaire = "";


  AfficherInventaire(String inventaire){
    this.inventaire = inventaire;
  }

  public String toString(){
    return inventaire;
  }
}
