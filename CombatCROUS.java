import java.util.Scanner;
import java.util.Random;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class CombatCROUS extends Dialogue implements Combat{

  private int PV;
  private int PVmax;
  private int PVmonstre;
  private int PVmaxmonstre;
  private int degats;
  private int degatsmonstre;

  public CombatCROUS(int pv) {
    this.PV = pv;
    this.PVmax = 25;
    this.PVmaxmonstre = 15;
    this.PVmonstre = PVmaxmonstre;
  }

  public int RetournePV(){
    return PV;
  }

    public int attaquer(){
        Random random = new Random();
        int degats = random.nextInt(6);
        return degats;
    }

    public int subir(){
        Random random = new Random();
        int degats = random.nextInt(6);
        return degats;
    }

    public boolean TentativeAttaque() {
      Random random = new Random();
      int test = random.nextInt(2);
      if (test == 1){
        return true;
      }else{
        return false;
      }
    }

    public void commencerCombat(){
      int i = 0;
      System.out.println("Connaissez vous le code pour les rouleaux de papiers toilettes ?");
      Scanner sc = new Scanner(System.in);
      String s = sc.nextLine();
      if(s.equals("papierCOVID-19")) {i = 5;
        dialogueCombat(i);}
        else Combat(0);
    }

    public void dialogueCombat(int i){
      if (i>0){
        String filePath = "./textes/dialcombat.txt";
        System.out.println(Read.readLineByLine(filePath));
      }
        choisirCombatOuRouleau(i);
    }

    public void choisirCombatOuRouleau(int i){
      if (i>0){
        Scanner sc=new Scanner(System.in);
        int choix = sc.nextInt();
        switch (choix) {
            case 1:
            Combat(i);
            break;
            case 2:
            lancerRouleau(i);
            break;
        }
      }else Combat(0);
    }

    public void dialogueCombatVous(int degats){

        System.out.println("Vous avez subi " +degats+ " point(s) de degats\n");
        System.out.println("");
        System.out.println("Vous avez  " +PV+ "/"+ PVmax+ "\n");
        System.out.println("");
    }

    public void dialogueCombatSecretaire(int degatsmonstre){

        System.out.println("Votre adversaire a subi " +degatsmonstre+ " point(s) de degats\n");
        System.out.println("");
        System.out.println("Elle est a " +PVmonstre+ "/"+ PVmaxmonstre+"\n");
        System.out.println("");
    }

    public void duel(){
        combatVous();
        combatMonstre();
    }

    public void combatVous(){
      System.out.println("Vous subissez une attaque");
      System.out.println("");
      if(TentativeAttaque()){
        System.out.println("Attaque adversaire reussie");
        System.out.println("");
        degats = subir();
        PV = PV - degats;
        dialogueCombatVous(degats);
    }else{
      System.out.println("Attaque echouee");
      System.out.println("");
      }
    }

    public void combatMonstre(){
      System.out.println("Vous realisez une attaque");
      System.out.println("");
      if(TentativeAttaque()){
        System.out.println("Attaque reussie");
        System.out.println("");
        degatsmonstre = attaquer();
        PVmonstre = PVmonstre - degatsmonstre;
        dialogueCombatSecretaire(degatsmonstre);
    }else{
      System.out.println("Attaque echoue");
      System.out.println("");
      }
    }


    public void Combat(int i){
        System.out.println("Combat face a face");
        System.out.println("");
        duel();
        if(PV<=0){
            declencherSequenceEchec();
        }
        else if(PVmonstre<=0){
            declencherSequenceReussite(RetournePV());
        }
        else
            dialogueCombat(i);
        }

    public void lancerRouleau(int i){
      i = i-1;
      System.out.println("Vous lancez un rouleau de papier");
      System.out.println("");
      combatMonstre();
      if(PV<=0){
          declencherSequenceEchec();
      }
      else if(PVmonstre<=0){
          declencherSequenceReussite(RetournePV());
      }
      else
          dialogueCombat(i);
      }

      public void declencherSequenceEchec(){
        System.out.println("Vous avez perdu");
        System.out.println("");
          (new Mort()).GameOver();
      }

      public void declencherSequenceReussite(int PV){
        p.setPV(PV);
        System.out.println("Vous avez gagne");
        System.out.println("Vous avez actuellement "+p.getPV()+"/"+p.getMaxPV()+"PV");
        System.out.println("");
        String vic = "./textes/dial_crous/crous_Victoire.txt";
        System.out.println(Read.readLineByLine(vic));
        (new DialogueCouloir()).choisirSalle(p.getPV());

      }

    }
