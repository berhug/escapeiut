import java.util.Scanner;
import java.util.Random;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import javax.swing.JFrame;
import java.awt.Dimension;

public class DialogueBureauProf extends Dialogue {

  public void Intro_Bureau_Prof(int PV){
    String entree = "./textes/dial_bur_prof/dial_entree_ask.txt";
    System.out.println(Read.readLineByLine(entree));
    p.setPV(PV);
    Scanner sc = new Scanner(System.in);
    int choix = sc.nextInt();
    switch(choix){
      case 1:
        String defonce = "./textes/dial_bur_prof/dial_defonce.txt";
        System.out.println(Read.readLineByLine(defonce));
        choisirBureau(p.getPV());
        break;
      case 2:
        String insulte = "./textes/dial_bur_prof/dial_insulte.txt";
        System.out.println(Read.readLineByLine(insulte));
        Intro_Bureau_Prof(p.getPV());
        break;
      case 3:
        String philo = "./textes/dial_bur_prof/dial_philo.txt";
        System.out.println(Read.readLineByLine(philo));
        Intro_Bureau_Prof(p.getPV());
        break;
      case 4:
        String poignee = "./textes/dial_bur_prof/dial_poignee.txt";
        System.out.println(Read.readLineByLine(poignee));
        choisirBureau(p.getPV());
        break;
      default:
        System.out.println("Choix non reconnu");
        Intro_Bureau_Prof(p.getPV());
        break;
      }
    }

      public void choisirBureau(int PV){
        p.setPV(PV);
        String entree = "./textes/dial_bur_prof/dial_bureau_ask.txt";
        System.out.println(Read.readLineByLine(entree));
        Scanner sc = new Scanner(System.in);
        int choix = sc.nextInt();
        switch(choix){
          case 1:
            String degrade = "./textes/dial_bur_prof/dial_poignee.txt";
            System.out.println(Read.readLineByLine(degrade));
            choisirBureau(p.getPV());
            break;
          case 2:
            String fouille = "./textes/dial_bur_prof/dial_fouille.txt";
            System.out.println(Read.readLineByLine(fouille));
            choixFouille(p.getPV());
            break;
          case 3:
            String sortir = "./textes/dial_bur_prof/dial_sortir.txt";
            System.out.println(Read.readLineByLine(sortir));
            (new DialogueCouloir()).choisirSalle(p.getPV());
            break;
          case 4 :
            int salle = 12;
            JFrame fenetre = new JFrame ("Plan de l'IUT :");
            Plan dessin = new Plan(salle);
            dessin.setPreferredSize(new Dimension (1200,480));
            fenetre.setContentPane(dessin);
            fenetre.pack();
            fenetre.setVisible(true);
            choisirBureau(p.getPV());
            break;
          default:
            System.out.println("Choix non reconnu");
            choisirBureau(p.getPV());
            break;
          }
        }

    public void choixFouille(int PV){
      p.setPV(PV);
      String vodka = "./textes/dial_bur_prof/dial_vodka_ask.txt";
      System.out.println(Read.readLineByLine(vodka));
      Scanner sc = new Scanner(System.in);
      int choix = sc.nextInt();
      switch(choix){
        case 1:
          String boire = "./textes/dial_bur_prof/dial_boire.txt";
          System.out.println(Read.readLineByLine(boire));
          p.setPV(25);
          choixMail(p.getPV());
          break;
        case 2:
          String inserer = "./textes/dial_bur_prof/dial_inserer_inv.txt";
          System.out.println(Read.readLineByLine(inserer));
          p.getInv().Liste_inventaire(2);
          choixMail(p.getPV());
          break;
        default:
          System.out.println("Choix non reconnu");
          choixFouille(p.getPV());
          break;
        }
      }

      public void choixMail(int PV){
        p.setPV(PV);
        String vodka = "./textes/dial_bur_prof/dial_mail_ask.txt";
        System.out.println(Read.readLineByLine(vodka));
        Scanner sc = new Scanner(System.in);
        int choix = sc.nextInt();
        switch(choix){
          case 1:
            String mail1 = "./textes/dial_bur_prof/dial_mail.txt";
            System.out.println(Read.readLineByLine(mail1));
            choisirCompteur(p.getPV());
            break;
          case 2:
            String sortir2 = "./textes/dial_bur_prof/dial_sortir.txt";
            System.out.println(Read.readLineByLine(sortir2));
            (new DialogueCouloir()).choisirSalle(p.getPV());
            break;
          default:
            System.out.println("Choix non reconnu");
            choixMail(p.getPV());
            break;
          }
        }

        public void choisirCompteur(int PV){
          p.setPV(PV);
          String vodka = "./textes/dial_bur_prof/dial_compteur_ask.txt";
          System.out.println(Read.readLineByLine(vodka));
          Scanner sc = new Scanner(System.in);
          int choix = sc.nextInt();
          switch(choix){
            case 1:
              String compteur = "./textes/dial_bur_prof/dial_compteur.txt";
              System.out.println(Read.readLineByLine(compteur));
              String sortir = "./textes/dial_bur_prof/dial_sortir.txt";
              System.out.println(Read.readLineByLine(sortir));
              (new DialogueCouloir()).choisirSalle(p.getPV());
              break;
            case 2:
              String sortir2 = "./textes/dial_bur_prof/dial_sortir.txt";
              System.out.println(Read.readLineByLine(sortir2));
              (new DialogueCouloir()).choisirSalle(p.getPV());
              break;
            default:
              System.out.println("Choix non reconnu");
              choixMail(p.getPV());
              break;
            }
          }



  }
