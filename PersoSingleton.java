/**
 * @author asjos
 */

public class PersoSingleton
{
    private static PersoSingleton instance = new PersoSingleton("inconnu",25);
    private String nom;
    private int pv;
    private int MaxPV;
    private Inventaire inv;

    /**
     * @param nom
     * @param pV
     */

    public PersoSingleton(String nom, int PV) {
      this.nom = nom;
      this.MaxPV = 25;
      this.pv = this.MaxPV;
      inv = new Inventaire();
    }

    private static PersoSingleton getInstance()
    {
        return (instance);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String s){
        this.nom = s;
    }

    public int getPV() {
        return this.pv;
    }

    public void setPV(int nb){
        this.pv = nb;
    }

    public int getMaxPV() {
        return this.MaxPV;
    }

    public void setMaxPV(int nb){
        if(nb>this.pv)
            this.MaxPV = nb;
        else
            this.MaxPV = this.pv;
    }

    public Inventaire getInv(){
      return inv;
    }

    /*public void obtenirNom(){
      System.out.print("Choisissez votre nom : ");
      (new DialoguePerso()).choisirNom();
    }*/


}
