import java.util.Scanner;
import java.util.Random;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DialogueBureauIUT extends Dialogue {


    public void Intro_Bureau_IUT(int PV) {
      p.setPV(PV);
      String bureauIUT = "./textes/dial_bureauIUT/Intro_BureauIut.txt";
      System.out.println(Read.readLineByLine(bureauIUT));
      inserer();
     }

    public void inserer(){
      System.out.println("Connaissez-vous les codes ?");
      System.out.println("Quel est le code pour le RGPD ? ");
      Scanner sc = new Scanner(System.in);
      String codeRGPD = sc.nextLine();
      if(codeRGPD.equals("rgpdTENIB")){
        System.out.println("Vous inserez le RGPD");
        System.out.println("Le pont de papier entre sans probleme. Comment ce coffre pouvait-il prévoir la forme et la taille de ce pont en papier ?");
        System.out.println("Quel est le code pour le pont de papier ?");
        String codePont = sc.nextLine();
        if(codePont.equals("pontTENODAL")){
          System.out.println("Vous inserez le pont de papier");
          System.out.println("Le livre de droit sur le RGPD entre parfaitement.");
          System.out.println("Quel est le code pour la cle USB ?");
          String codeCle = sc.nextLine();
          if(codeCle.equals("sudokuLEGEOR")){
            System.out.println("Vous inserez la clé USB");
            System.out.println("Ce petit rectangle rendre dedans, il fait si petit par rapport aux autres");
            System.out.println("Quel est le code pour le justificatif d'absence ?");
            String codeJustificatif = sc.nextLine();
            if(codeJustificatif.equals("justificatifEGAIRAM")){
              System.out.println("Vous insérez le justificatif");
              System.out.println("Ce trou fin est assez bien construit, on insere la feuille horizontalement et il l'avale.");
              System.out.println("Félicitations, vous avez récupéré les quatre objets et vous les avez inséré dans le coffre-fort.");
              apparitionCle(p.getPV());
            }else (new DialogueCouloir()).choisirSalle(p.getPV());
          }else (new DialogueCouloir()).choisirSalle(p.getPV());
        }else (new DialogueCouloir()).choisirSalle(p.getPV());
      }else (new DialogueCouloir()).choisirSalle(p.getPV());

      }


      public void apparitionCle(int PV){
        p.setPV(PV);
        String bureauIUTAsk = "./textes/dial_bureauIUT/BureauIut_ask.txt";
        System.out.println(Read.readLineByLine(bureauIUTAsk));
        Scanner sc = new Scanner(System.in);
        int choix = sc.nextInt();
        switch(choix){
          case 1 :
            String answer1 = "./textes/dial_bureauIUT/BureauIut_ask_rep1.txt";
            System.out.println(Read.readLineByLine(answer1));
            CombatBossFinal(p.getPV());
            break;
          case 2 :
            String answer2 = "./textes/dial_bureauIUT/BureauIut_ask_rep2.txt";
            System.out.println(Read.readLineByLine(answer2));
            String answer3 = "./textes/dial_bureauIUT/BureauIut_ask_rep1.txt";
            System.out.println(Read.readLineByLine(answer3));
            CombatBossFinal(p.getPV());
            break;
          default:
            System.out.println("Choix non reconnu");
            apparitionCle(p.getPV());
            break;
        }
      }

      public void CombatBossFinal(int PV){
        p.setPV(PV);
        String answer1 = "./textes/dial_bureauIUT/intro_dial_fuite.txt";
        System.out.println(Read.readLineByLine(answer1));
        System.out.println("Vous pouvez faire jusqu'a 6 points de degats et vous possedez actuellement " +p.getPV()+ "PV\n");
        String answer2 = "./textes/dial_bureauIUT/intro_dial_fuite2.txt";
        System.out.println(Read.readLineByLine(answer2));
        Scanner sc = new Scanner(System.in);
        int choix = sc.nextInt();
        switch(choix){
          case 1 :
            String dingue = "./textes/dial_bureauIUT/dingue.txt";
            System.out.println(Read.readLineByLine(dingue));
            (new CombatTENODAL(p.getPV())).commencerCombat();
            break;
          case 2 :
            System.out.println("Que la fuite commence !!!");
            (new FuiteFinale()).commencerFuiteFinale(p.getPV());
            break;
          default :
            System.out.println("Choix noin reconnu");
            CombatBossFinal(p.getPV());
            break;
          }
        }
}
