import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import javax.swing.JFrame;
import java.awt.Dimension;

public class DialogueBibliotheque extends Dialogue {

  public void Intro_Bibliotheque_IUT(int PV) {
    p.setPV(PV);
    String Intro_Bibliotheque_IUT = "./textes/dial_biblio/intro.txt";
    System.out.println(Read.readLineByLine(Intro_Bibliotheque_IUT));
    Scanner sc = new Scanner(System.in);
    int choix = sc.nextInt();
      switch (choix) {
        case 1:
          String biblio1 = "./textes/dial_biblio/intro.txt";
          System.out.println(Read.readLineByLine(biblio1));
          (new DialoguePetitCouloir()).Petit_Couloir(false, p.getPV());
          break;
        case 2:
          System.out.println("Le livre sur le RGBD se trouve dorénavant dans votre inventaire");
          p.getInv().Liste_inventaire(3);
          (new DialoguePetitCouloir()).Petit_Couloir(true, p.getPV());
          break;
        case 3:
          int salle = 15;
          JFrame fenetre = new JFrame ("Plan de l'IUT :");
          Plan dessin = new Plan(salle);
          dessin.setPreferredSize(new Dimension (1200,480));
          fenetre.setContentPane(dessin);
          fenetre.pack();
          fenetre.setVisible(true);
          Intro_Bibliotheque_IUT(p.getPV());
          break;
        default:
          System.out.println("Choix non reconnu");
          Intro_Bibliotheque_IUT(p.getPV());
          break;

        }
      }




}
