import java.util.Scanner;
import java.util.Random;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import javax.swing.JFrame;
import java.awt.Dimension;

public class DialogueSallePC extends Dialogue {

  public void Intro_Salle_Informatique(int PV){
    p.setPV(PV);
    String informatique = "./textes/dial_PC/dial_PC.txt";
    System.out.println(Read.readLineByLine(informatique));
    choisir1(p.getPV());
  }

  public void choisir1(int PV){
    p.setPV(PV);
    String choisir = "./textes/dial_PC/dial_ask.txt";
    System.out.println(Read.readLineByLine(choisir));
    Scanner sc = new Scanner(System.in);
    int choix = sc.nextInt();
    switch(choix){
      case 1 :
        String eleve = "./textes/dial_PC/dial_eleve.txt";
        System.out.println(Read.readLineByLine(eleve));
        choisir2(p.getPV());
        break;
      case 2 :
        String fouille = "./textes/dial_PC/dial_fouille.txt";
        System.out.println(Read.readLineByLine(fouille));
        choisirCle1(p.getPV());
        break;
      case 3 :
        String piratage = "./textes/dial_PC/dial_piratage.txt";
        System.out.println(Read.readLineByLine(piratage));
        choisir1(p.getPV());
        break;
      case 4 :
        System.out.println("Vous sortez de la salle PC");
        System.out.println("");
        (new DialogueCouloir()).choisirSalle(p.getPV());
        break;
      case 5 :
        int salle = 8;
        JFrame fenetre = new JFrame ("Plan de l'IUT :");
        Plan dessin = new Plan(salle);
        dessin.setPreferredSize(new Dimension (1200,480));
        fenetre.setContentPane(dessin);
        fenetre.pack();
        fenetre.setVisible(true);
        choisir1(p.getPV());
        break;
      default :
        String nonreconnu = "./textes/choix_non_reconnu.txt";
        System.out.println(Read.readLineByLine(nonreconnu));
        choisir1(p.getPV());
        break;
    }
  }

    public void choisir2(int PV){
      p.setPV(PV);
      String choisir = "./textes/dial_PC/dial_ask2.txt";
      System.out.println(Read.readLineByLine(choisir));
      Scanner sc = new Scanner(System.in);
      int choix = sc.nextInt();
      switch(choix){
        case 1 :
          String fouille = "./textes/dial_PC/dial_fouille.txt";
          System.out.println(Read.readLineByLine(fouille));
          choisirCle2(p.getPV());
          break;
        case 2 :
          String piratage = "./textes/dial_PC/dial_piratage.txt";
          System.out.println(Read.readLineByLine(piratage));
          choisir2(p.getPV());
          break;
        case 3 :
          System.out.println("Vous sortez de la salle PC");
          System.out.println("");
          (new DialogueCouloir()).choisirSalle(p.getPV());
          break;
        default :
          String nonreconnu = "./textes/choix_non_reconnu.txt";
          System.out.println(Read.readLineByLine(nonreconnu));
          choisir2(p.getPV());
          break;
        }
      }

    public void choisirCle1(int PV){
      p.setPV(PV);
      String Cle = "./textes/dial_PC/dial_Cle.txt";
      System.out.println(Read.readLineByLine(Cle));
      Scanner sc = new Scanner(System.in);
      int choix = sc.nextInt();
      switch(choix){
        case 1 :
          System.out.println("La cle USB se trouve dorenavant dans votre inventaire");
          (new Inventaire()).Liste_inventaire(5);
          String legeor1 = "./textes/dial_PC/dial_legeor1.txt";
          System.out.println(Read.readLineByLine(legeor1));
          legeor1(p.getPV());
          break;
        case 2 :
          choisir1(p.getPV());
          break;
        default :
          String nonreconnu = "./textes/choix_non_reconnu.txt";
          System.out.println(Read.readLineByLine(nonreconnu));
          choisir1(p.getPV());
          break;
      }
    }

      public void choisirCle2(int PV){
        p.setPV(PV);
        String Cle = "./textes/dial_PC/dial_Cle.txt";
        System.out.println(Read.readLineByLine(Cle));
        Scanner sc = new Scanner(System.in);
        int choix = sc.nextInt();
        switch(choix){
          case 1 :
            System.out.println("La clé USB se trouve dorénavant dans votre inventaire");
            p.getInv().Liste_inventaire(5);
            String legeor1 = "./textes/dial_PC/dial_legeor1.txt";
            System.out.println(Read.readLineByLine(legeor1));
            legeor1(p.getPV());
            break;
          case 2 :
            choisir2(p.getPV());
            break;
          default :
            String nonreconnu = "./textes/choix_non_reconnu.txt";
            System.out.println(Read.readLineByLine(nonreconnu));
            choisir2(p.getPV());
            break;
        }
    }

    public void legeor1(int PV){
      p.setPV(PV);
      String Cle = "./textes/dial_PC/dial_legeor_ask1.txt";
      System.out.println(Read.readLineByLine(Cle));
      Scanner sc = new Scanner(System.in);
      int choix = sc.nextInt();
      switch(choix){
        case 1 :
          String legeor = "./textes/dial_PC/dial_legeor2.txt";
          System.out.println(Read.readLineByLine(legeor));
          legeor2(p.getPV());
          break;
        case 2 :
          System.out.println("VOUS : Bah dans les faits le compilateur Java accepte les deux langues donc on peut très bien rédiger en anglais et en français.");
          baisserPV();
          if (p.getPV() > 0){
            String legeor2 = "./textes/dial_PC/dial_legeor2.txt";
            System.out.println(Read.readLineByLine(legeor2));
            legeor2(p.getPV());
          }
          break;
        case 3:
          System.out.println("VOUS : Ok mais dans le sujet Sudoku, vous avez dit que la fonction récursive s’appelait remplirGrille ALORS QUE le fichier java s’appelle SudokuAll. Vous aimez les contradictions ?");
          baisserPV();
          if (p.getPV() > 0){
            String legeor3 = "./textes/dial_PC/dial_legeor2.txt";
            System.out.println(Read.readLineByLine(legeor3));
            legeor2(p.getPV());
          }
          break;
          default:
            String nonreconnu = "./textes/choix_non_reconnu.txt";
            System.out.println(Read.readLineByLine(nonreconnu));
            legeor1(p.getPV());
            break;
        }
    }

    public void legeor2(int PV){
      p.setPV(PV);
      String Cle = "./textes/dial_PC/dial_legeor_ask2.txt";
      System.out.println(Read.readLineByLine(Cle));
      Scanner sc = new Scanner(System.in);
      int choix = sc.nextInt();
      switch(choix){
        case 1 :
          String legeor4 = "./textes/dial_PC/dial_legeor3.txt";
          System.out.println(Read.readLineByLine(legeor4));
          (new DialogueCouloir()).choisirSalle(p.getPV());
          break;
        case 2 :
          System.out.println("VOUS : Bah dans les faits cela peut poser un problème dans certains compilateurs parce qu’ils ne sont pas mis à jour. Et dans les autres langages, les accents ne sont pas autorisés.");
          baisserPV();
          if (p.getPV() > 0) {
            String legeor5 = "./textes/dial_PC/dial_legeor3.txt";
            System.out.println(Read.readLineByLine(legeor5));
            (new DialogueCouloir()).choisirSalle(p.getPV());
          }
          break;
        default:
          String nonreconnu = "./textes/choix_non_reconnu.txt";
          System.out.println(Read.readLineByLine(nonreconnu));
          legeor2(p.getPV());
          break;
      }
    }

    public void baisserPV(){
      Random random = new Random();
      int nb = random.nextInt(6);
      p.setPV(p.getPV()-nb);
      System.out.println("Vous avez pris "+nb+" point(s) de degats, il vous reste " + p.getPV() + "/" + p.getMaxPV() + " PV.");
      if (p.getPV() <= 0)
      {
        (new Mort()).GameOver();
    }
  }



}
