import java.util.Scanner;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import javax.swing.JFrame;
import java.awt.Dimension;


public class DialogueCROUS extends Dialogue
{
  public void Intro_Crous(int PV)
  {
    p.setPV(PV);
    String intro = "./textes/dial_crous/crous_intro.txt";
    System.out.println(Read.readLineByLine(intro));
    Commande(p.getPV());
  }

  public void Commande(int PV)
  {
    p.setPV(PV);
    String intro = "./textes/dial_crous/crous_ask.txt";
    System.out.println(Read.readLineByLine(intro));
    Scanner sc = new Scanner(System.in);
    int choix = sc.nextInt();

    switch(choix){
      case 1 :
      String salami = "./textes/dial_crous/crous_ask_rep1.txt";
      System.out.println(Read.readLineByLine(salami));
      Paiement(p.getPV());
      break;
      case 2 :
      String pizza = "./textes/dial_crous/crous_ask_rep2.txt";
      System.out.println(Read.readLineByLine(pizza));
      Paiement(p.getPV());
      break;
      case 3 :
      String hamburger = "./textes/dial_crous/crous_ask_rep3.txt";
      System.out.println(Read.readLineByLine(hamburger));
      Paiement(p.getPV());
      break;
      case 4 :
      String taco = "./textes/dial_crous/crous_ask_rep4.txt";
      System.out.println(Read.readLineByLine(taco));
      Paiement(p.getPV());
      break;
      default :
      System.out.println("Choix non reconnu");
      Commande(p.getPV());
      break;
    }
  }

  public void Paiement(int PV)
  {
    p.setPV(PV);
    String paiement = "./textes/dial_crous/crous_ask2.txt";
    System.out.println(Read.readLineByLine(paiement));
    Scanner sc = new Scanner(System.in);
    int choix = sc.nextInt();

    switch(choix){
      case 1 :
      String billet = "./textes/dial_crous/crous_ask2_rep1.txt";
      System.out.println(Read.readLineByLine(billet));
      Billet(p.getPV());
      break;
      case 2 :
      String cb = "./textes/dial_crous/crous_ask2_rep2.txt";
      System.out.println(Read.readLineByLine(cb));
      (new DialogueCouloir()).choisirSalle(p.getPV());
      break;
      case 3 :
      String cheque = "./textes/dial_crous/crous_ask2_rep3.txt";
      System.out.println(Read.readLineByLine(cheque));
      (new DialogueCouloir()).choisirSalle(p.getPV());
      break;
      case 4 :
      String ticket = "./textes/dial_crous/crous_ask2_rep4.txt";
      System.out.println(Read.readLineByLine(ticket));
      (new DialogueCouloir()).choisirSalle(p.getPV());
      break;
      default :
      System.out.println("Choix non reconnu");
      Paiement(p.getPV());
      break;
    }
  }

  public void Billet(int PV)
  {
    p.setPV(PV);
    String billet = "./textes/dial_crous/crous_ask3.txt";
    System.out.println(Read.readLineByLine(billet));
    Scanner sc = new Scanner(System.in);
    int flooz = sc.nextInt();

    switch(flooz){
      case 1 :
      String ans1 = "./textes/dial_crous/crous_ask3_rep1.txt";
      System.out.println(Read.readLineByLine(ans1));
      (new DialogueCouloir()).choisirSalle(p.getPV());
      break;
      case 2 :
      String ans2 = "./textes/dial_crous/crous_ask3_rep2.txt";
      System.out.println(Read.readLineByLine(ans2));
      (new CombatCROUS(p.getPV())).commencerCombat();
      break;
      default :
      System.out.println("Choix non reconnu");
      Billet(p.getPV());
      break;
    }
  }
}
