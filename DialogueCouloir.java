import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import javax.swing.JFrame;
import java.awt.Dimension;
import java.io.*;

public class DialogueCouloir extends Dialogue{

    public void choisirSalle(int PV){
      p.setPV(PV);
      String choisirSalle = "./textes/dial_couloir/choisir_salle.txt";
      System.out.println(Read.readLineByLine(choisirSalle));
      Scanner sc = new Scanner(System.in);
      int choix = sc.nextInt();
        switch (choix) {
          case 1:
            (new DialogueSallePC()).Intro_Salle_Informatique(p.getPV());
            break;
          case 2:
            (new DialogueBureauProf()).Intro_Bureau_Prof(p.getPV());
            break;
          case 3:
            (new DialogueSalle105()).Intro_Salle105(p.getPV());
            break;
          case 4:
            (new DialogueToilettes()).Intro_Toilettes(p.getPV());
            break;
          case 5:
            for(AfficherInventaire lang : AfficherInventaire.values()){
              System.out.println(lang);
            }
            choisirSalle(p.getPV());
            break;
          case 6:
            (new DialogueCafeteriat()).Cafeteria(p.getPV());
            break;
          case 7:
            (new DialogueBibliotheque()).Intro_Bibliotheque_IUT(p.getPV());
            break;
          case 8:
            (new DialogueSecretariat()).Intro_Secretariat(p.getPV());
            break;
          case 9:
            (new DialogueCROUS()).Intro_Crous(p.getPV());
            break;
          case 10:
            int salle = 1;
            JFrame fenetre = new JFrame ("Plan de l'IUT :");
            Plan dessin = new Plan(salle);
            dessin.setPreferredSize(new Dimension (1200,480));
            fenetre.setContentPane(dessin);
            fenetre.pack();
            fenetre.setVisible(true);
            choisirSalle(p.getPV());
            break;
          case 11:
            (new QuitterJeu()).quitter();
          default:
            System.out.println("Choix non reconnu");
            choisirSalle(p.getPV());
            break;
        }
      }
    }
