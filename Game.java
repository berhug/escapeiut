import java.io.IOException;
import java.io.InputStream;
import java.nio.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import javafx.application.*;
import javafx.animation.*;
import javafx.stage.*;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.*;
import javafx.scene.input.KeyCode;
import javafx.util.Duration;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Glow;
import javafx.scene.effect.GaussianBlur;

public class Game extends Application {
    public PersoSingleton p;
    public GameMenu gamemenu;
    public Stage stage;
    public Scene scene;

    public void setStage(Stage stage){
      this.stage = stage;
    }

    public Stage getStage(){
      return stage;
    }

    public void setScene(Scene scene){
      this.scene = scene;
    }

    public Scene getScene(){
      return scene;
    }

    public Game(){
        this.p = new PersoSingleton("inconnu",25);
    }

    public static void main(String [] args){
      PersoSingleton p = new PersoSingleton("inconnu",25);
      launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException {

      Pane root = new Pane();
      root.setPrefSize(900,700);
      Scene scene = new Scene(root);
      gamemenu = new GameMenu();
      gamemenu.setVisible(false);


      InputStream is = Files.newInputStream(Paths.get("images/lancement.jpg"));
      Image img = new Image(is);
      is.close();
      ImageView imgV = new ImageView(img);
      imgV.setFitHeight(700);
      imgV.setFitWidth(900);


      root.getChildren().addAll(imgV, gamemenu);

      //scene.setOnKeyPressed(event -> {
        //if(event.getCode() == KeyCode.ESCAPE) {
          //if (!gamemenu.isVisible()){
            FadeTransition fti = new FadeTransition(Duration.seconds(0.75),gamemenu);
            fti.setFromValue(0);
            fti.setToValue(1);
            fti.setOnFinished(evt -> gamemenu.setVisible(true));
            fti.play();
          /*}else{
            FadeTransition ft = new FadeTransition(Duration.seconds(0.75),gamemenu);
            ft.setFromValue(0);
            ft.setToValue(1);
            ft.setOnFinished(evt -> gamemenu.setVisible(false));
            ft.play();*/
          //}
      //  }
      //});
      setStage(stage);
      setScene(scene);
      stage.setScene(scene);
      stage.show();

    }

    public class GameMenu extends Parent {
      GameMenu(){
        Pane root = new Pane();
        Pane root2 = new Pane();
        Pane root3 = new Pane();

        Rectangle rt = new Rectangle(900,700);
        //rt.setFill(Color.WHITE);
        rt.setOpacity(0);

        root.setPrefSize(900,700);
        VBox menu = new VBox(10);
        VBox menu2 = new VBox(10);
        VBox menu3 = new VBox(10);

        menu.setTranslateX(600);
    		menu.setTranslateY(350);
        menu2.setTranslateX(600);
        menu2.setTranslateY(350);
        menu3.setTranslateX(600);
        menu3.setTranslateY(350);

        final int offset = 1000;
        menu2.setTranslateX(offset);

        Button resume = new Button("Demarrer");
        resume.setOnMouseClicked (event -> {
          getChildren().add(menu3);

          TranslateTransition tt = new TranslateTransition(Duration.seconds(0.1), menu);
          tt.setToX(menu.getTranslateX()-offset);

          TranslateTransition tti = new TranslateTransition(Duration.seconds(0.1), menu3);
          tti.setToX(menu.getTranslateX());

          tt.play();
          tti.play();

          tt.setOnFinished(evt -> {
            getChildren().remove(menu);
          });
          //(new DialogueJeu()).intro();
        });
        Button credits = new Button("Credits");
        credits.setOnMouseClicked (event -> {
          getChildren().add(menu2);

          TranslateTransition tt = new TranslateTransition(Duration.seconds(0.1), menu);
          tt.setToX(menu.getTranslateX()-offset);

          TranslateTransition tti = new TranslateTransition(Duration.seconds(0.1), menu2);
          tti.setToX(menu.getTranslateX());

          tt.play();
          tti.play();

          tt.setOnFinished(evt -> {
            getChildren().remove(menu);
          });
          String filePath2 = "./textes/demarrez/credits.txt";
          System.out.println(Read.readLineByLine(filePath2));
        });
        Button objectif = new Button("Objectif");
        objectif.setOnMouseClicked (event -> {
          getChildren().add(menu2);

          TranslateTransition tt = new TranslateTransition(Duration.seconds(0.1), menu);
          tt.setToX(menu.getTranslateX()-offset);

          TranslateTransition tti = new TranslateTransition(Duration.seconds(0.1), menu2);
          tti.setToX(menu.getTranslateX());

          tt.play();
          tti.play();

          tt.setOnFinished(evt -> {
            getChildren().remove(menu);
          });
          String filePath2 = "./textes/demarrez/objectif.txt";
          System.out.println(Read.readLineByLine(filePath2));
        });
        Button copyright = new Button("Copyright");
        copyright.setOnMouseClicked (event -> {
          getChildren().add(menu2);

          TranslateTransition tt = new TranslateTransition(Duration.seconds(0.1), menu);
          tt.setToX(menu.getTranslateX()-offset);

          TranslateTransition tti = new TranslateTransition(Duration.seconds(0.1), menu2);
          tti.setToX(menu.getTranslateX());

          tt.play();
          tti.play();

          tt.setOnFinished(evt -> {
            getChildren().remove(menu);
          });
          String filePath2 = "./textes/demarrez/copyright.txt";
          System.out.println(Read.readLineByLine(filePath2));
        });
        Button exit = new Button("Quitter");
        exit.setOnMouseClicked(event -> {
          System.exit(0);
        });
        Button retour = new Button("<- Retour");
        retour.setOnMouseClicked(event -> {
          getChildren().add(menu);

          TranslateTransition tt = new TranslateTransition(Duration.seconds(0.1), menu2);
          tt.setToX(menu2.getTranslateX()+offset);

          TranslateTransition tti = new TranslateTransition(Duration.seconds(0.1), menu);
          tti.setToX(menu2.getTranslateX());

          tt.play();
          tti.play();

          tt.setOnFinished(evt -> {
            getChildren().remove(menu2);
          });
        });
        Button facile = new Button ("Facile (100% PV)");
        facile.setOnMouseClicked(event -> {
          FadeTransition ft = new FadeTransition(Duration.seconds(0.1), gamemenu);
          ft.setFromValue(1);
          ft.setToValue(0);
          ft.setOnFinished(evt -> gamemenu.setVisible(true));
          ft.play();
          stage.close();
          (new DialogueTenib()).AppelTenib(25);
        });
        Button moyen = new Button ("Moyen (60% PV)");
        moyen.setOnMouseClicked(event -> {
          FadeTransition ft = new FadeTransition(Duration.seconds(0.1), gamemenu);
          ft.setFromValue(1);
          ft.setToValue(0);
          ft.setOnFinished(evt -> gamemenu.setVisible(true));
          ft.play();
          stage.close();
          (new DialogueTenib()).AppelTenib(15);
        });
        Button difficile = new Button ("Difficile (40% PV)");
        difficile.setOnMouseClicked(event -> {
          FadeTransition ft = new FadeTransition(Duration.seconds(0.1), gamemenu);
          ft.setFromValue(1);
          ft.setToValue(0);
          ft.setOnFinished(evt -> gamemenu.setVisible(true));
          ft.play();
          stage.close();
          (new DialogueTenib()).AppelTenib(10);
        });
        Button diabolique = new Button ("Diabolique (20% PV)");
        diabolique.setOnMouseClicked(event -> {
          FadeTransition ft = new FadeTransition(Duration.seconds(0.1), gamemenu);
          ft.setFromValue(1);
          ft.setToValue(0);
          ft.setOnFinished(evt -> gamemenu.setVisible(true));
          ft.play();
          stage.close();
          (new DialogueTenib()).AppelTenib(5);
        });
        Button back = new Button ("<- Retour");
        back.setOnMouseClicked(event -> {
          getChildren().add(menu);

          TranslateTransition tt = new TranslateTransition(Duration.seconds(0.1), menu3);
          tt.setToX(menu3.getTranslateX()+offset);

          TranslateTransition tti = new TranslateTransition(Duration.seconds(0.1), menu);
          tti.setToX(menu3.getTranslateX());

          tt.play();
          tti.play();

          tt.setOnFinished(evt -> {
            getChildren().remove(menu3);
          });
        });



        menu.getChildren().addAll(resume,credits, objectif, copyright, exit);
        root.getChildren().addAll(rt, menu);
        menu2.getChildren().addAll(retour);
        root2.getChildren().addAll(rt, menu2);
        menu3.getChildren().addAll(facile, moyen , difficile, diabolique, back);
        root3.getChildren().addAll(rt, menu3);
        getChildren().addAll(root);
      }
    }

    public static class Button extends StackPane {
      Button(String name) {
        Text text = new Text(name);
        text.setFont(Font.font(20));
        text.setFill(Color.WHITE);

        Rectangle rt = new Rectangle(250, 30);
        rt.setOpacity(0.7);
        rt.setFill(Color.BLACK);
        rt.setEffect(new GaussianBlur(3.5));

        setAlignment(Pos.CENTER);
        setRotate(-0.5);
        getChildren().addAll(rt, text);

        setOnMouseEntered(event -> {
          rt.setFill(Color.GREY);
          text.setFill(Color.BLACK);
        });
        setOnMouseExited(event -> {
          rt.setFill(Color.BLACK);
          text.setFill(Color.DARKGREY);
        });


        DropShadow drop  = new DropShadow(50, Color.WHITE);
        drop.setInput(new Glow());

        setOnMousePressed(event -> {
          setEffect(drop);
          rt.setFill(Color.DARKVIOLET);
        });
        setOnMouseReleased(event -> {
          setEffect(null);
        });
      }
    }
}
