import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Inventaire {
    private boolean rouleau;
    private boolean vodka;
    private boolean RGBD;
    private boolean pont;
    private boolean usb;
    private boolean justificatif;

    public Inventaire() {
        rouleau = false;
        vodka = false;
        RGBD = false;
        pont = false;
        usb = false;
        justificatif = false;
    }

    public boolean getRouleau() {return rouleau;}
    public boolean getVodka() {return vodka;}
    public boolean getRGPD() {return RGBD;}
    public boolean getPont() {return pont;}
    public boolean getusb() {return usb;}
    public boolean getjustificatif() {return justificatif;}

    public void setRouleau(boolean b) {this.rouleau = b;}
    public void setVodka(boolean b) {this.vodka = b;}
    public void setRGPD(boolean b) {this.RGBD = b;}
    public void setPont(boolean b) {this.pont = b;}
    public void setusb(boolean b) {this.usb = b;}
    public void setjustificatif(boolean b) {this.justificatif = b;}


    

    public void Liste_inventaire(int e){
        switch (e) {
            case 1 :
            Inserer_rouleau_papier(5);
            break;
            case 2 :
            Inserer_Vodka();
            break;
            case 3 :
            Inserer_RGPD();
            break;
            case 4 :
            Inserer_pont_papier();
            break;
            case 5 :
            inserer_Cle_USB();
            break;
            case 6 :
            Inserer_Justificatif();
            break;
        }
    }

    public void Inserer_rouleau_papier(int i){

            System.out.println("Rouleau de papier toilette (X"+i+") : disponible dans les toilettes.");
            System.out.println("");
            System.out.println("Retenez bien ce code : papierCOVID-19");
            setObjet(1,true);
    }

    public void Inserer_Vodka(){

            System.out.println("Bouteille Vodka : c’est FOU ce qu'on peut trouver en fouillant le bureau des profs.");
            System.out.println("");
            setObjet(2,true);
    }

    public void Inserer_pont_papier(){

            System.out.println("Pont en papier : pas facile a fabriquer, en realite");
            System.out.println("");
            System.out.println("Retenez bien ce code : pontTENODAL");
            setObjet(4,true);
    }

    public void Inserer_RGPD(){

            System.out.println("Livre de loi sur le RGPD : extremement ennuyant, a quoi pourrait-il servir dans le cadre d'une formation d'informatique ?");
            System.out.println("");
            System.out.println("Retenez bien ce code : rgpdTENIB");
            setObjet(3,true);
    }

    public void inserer_Cle_USB(){

            System.out.println("Cle USB : contient le projet Sudoku.java");
            System.out.println("");
            System.out.println("Retenez bien ce code : sudokuLEGEOR");
            setObjet(5,true);
    }

    public void Inserer_Justificatif(){

            System.out.println("Justificatif d'absence : ne depassez JAMAIS les 4 absences injustifiees, ou vous en subirez les consequences.");
            System.out.println("");
            System.out.println("Retenez bien ce code : justificatifEGAIRAM");
            setObjet(6,true);
    }

    public boolean getObjet(int i){
      if(i == 1) return rouleau;
      else if (i == 2) return vodka;
      else if (i == 3) return RGBD;
      else if (i == 4) return pont;
      else if (i == 5) return usb;
      else if (i == 6) return justificatif ;
      else return false;
    }

    public void setObjet(int i, boolean b){
      if (i == 1) rouleau = b;
      else if (i == 2) vodka = b;
      else if (i == 3) RGBD = b;
      else if (i == 4) pont = b;
      else if (i == 5) usb = b;
      else if (i == 6) justificatif = b;
    }



}
