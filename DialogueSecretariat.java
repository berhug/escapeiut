import java.util.Scanner;
import javax.swing.JFrame;
import java.awt.Dimension;

public class DialogueSecretariat extends Dialogue {
    protected Scanner sc = new Scanner(System.in);

    public void Intro_Secretariat(int PV) {
        p.setPV(PV);
        String intro = "./textes/dial_secretariat/secretariat_intro.txt";
        System.out.println(Read.readLineByLine(intro));
        choixRep(p.getPV());
    }

    public void choixRep(int PV){
        p.setPV(PV);
        String choix = "./textes/dial_secretariat/dial_secretariat_ask.txt";
        System.out.println(Read.readLineByLine(choix));
        int choixrep = sc.nextInt();

        switch(choixrep){
            case 1 :
            String filePath1 = "./textes/dial_secretariat/dial_secretariat_rep_1.txt";
            System.out.println(Read.readLineByLine(filePath1));
            (new CombatSecretaire(p.getPV())).commencerCombat();
            break;
            case 2 :
            String filePath2 = "./textes/dial_secretariat/dial_secretariat_rep_2.txt";
            System.out.println(Read.readLineByLine(filePath2));
            (new CombatSecretaire(p.getPV())).commencerCombat();
            break;
            case 3 :
            String filePath3 = "./textes/dial_secretariat/dial_secretariat_rep_3.txt";
            System.out.println(Read.readLineByLine(filePath3));
            choix3(p.getPV());
            break;
            case 4 :
            String filePath4 = "./textes/dial_secretariat/dial_secretariat_rep_4.txt";
            System.out.println(Read.readLineByLine(filePath4));
            (new DialogueCouloir()).choisirSalle(p.getPV());
            break;
        }
    }

    public void choix3(int PV){
        p.setPV(PV);
        int choixrep = sc.nextInt();
        switch(choixrep){
            case 1 :
            String filePath1 = "./textes/dial_secretariat/dial_secretariat_rep_3_rep1.txt";
            System.out.println(Read.readLineByLine(filePath1));
            (new CombatSecretaire(p.getPV())).commencerCombat();
            break;
            case 2 :
            String filePath2 = "./textes/dial_secretariat/dial_secretariat_rep_3_rep2.txt";
            System.out.println(Read.readLineByLine(filePath2));
            (new CombatSecretaire(p.getPV())).commencerCombat();
            break;
            case 3 :
            String filePath3 = "./textes/dial_secretariat/dial_secretariat_rep_3_rep3.txt";
            System.out.println(Read.readLineByLine(filePath3));
            (new Inventaire()).Liste_inventaire(6);
            (new DialogueCouloir()).choisirSalle(p.getPV());
            break;
        }
    }

    public void Victoire(int PV){
      p.setPV(PV);
      Scanner sc = new Scanner(System.in);
      int choix = sc.nextInt();
      switch(choix){
        case 1 :
          String filePath3 = "./textes/dial_secretariat/dial_secretariat_avec_justificatif.txt";
          System.out.println(Read.readLineByLine(filePath3));
          p.getInv().Liste_inventaire(6);
          (new DialogueCouloir()).choisirSalle(p.getPV());
          break;
        case 2:
          String filePath = "./textes/dial_secretariat/dial_secretariat_sans_justificatif.txt";
          System.out.println(Read.readLineByLine(filePath));
          (new DialogueCouloir()).choisirSalle(p.getPV());
          break;
      }
    }


}
