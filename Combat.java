public interface Combat{

    public int RetournePV();

    public int attaquer();

    public int subir();

    public boolean TentativeAttaque();

    public void commencerCombat();

    public void dialogueCombat(int i);

    public void choisirCombatOuRouleau(int i);

    public void dialogueCombatVous(int degats);

    public void dialogueCombatSecretaire(int degatsmonstre);

    public void duel();

    public void combatVous();

    public void combatMonstre();

    public void Combat(int i);

    public void lancerRouleau(int i);

    public void declencherSequenceEchec();

    public void declencherSequenceReussite(int PV);

  }
