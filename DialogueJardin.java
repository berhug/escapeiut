import java.util.Scanner;
import java.io.IOException;
import java.util.Random;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import javax.swing.JFrame;
import java.awt.Dimension;

public class DialogueJardin extends Dialogue {


  public void choixJardin(int PV)
  {
     p.setPV(PV);
     String choixjardin = "./textes/dial_jardin/dial_fumer_ask.txt";
     System.out.println(Read.readLineByLine(choixjardin));
     Scanner sc = new Scanner(System.in);
     int choix_jardin = sc.nextInt();

     switch(choix_jardin){
          case 1 :
            String filePath1 = "./textes/dial_jardin/dial_fumer.txt";
            System.out.println(Read.readLineByLine(filePath1));
            choixJardin(p.getPV());
            break;
          case 2 :
            String filePath2 = "./textes/dial_jardin/dial_aventure.txt";
            System.out.println(Read.readLineByLine(filePath2));
            choix1(p.getPV());
            break;
          case 3 :
            String filePath3 = "./textes/dial_jardin/dial_rentrer.txt";
            System.out.println(Read.readLineByLine(filePath3));
            (new DialogueCafeteriat()).choixCornelien(p.getPV());
            break;
          case 4:
            int salle = 3;
            JFrame fenetre = new JFrame ("Plan de l'IUT :");
            Plan dessin = new Plan(salle);
            dessin.setPreferredSize(new Dimension (1200,480));
            fenetre.setContentPane(dessin);
            fenetre.pack();
            fenetre.setVisible(true);
            choixJardin(p.getPV());
            break;
          default:
            System.out.println("Choix non reconnu");
            choixJardin(p.getPV());
            break;
     }
  }

  public void choix1(int PV){
    p.setPV(PV);
    String choix1 = "./textes/dial_jardin/dial_choix1_ask.txt";
    System.out.println(Read.readLineByLine(choix1));
    Scanner sc = new Scanner(System.in);
    int choix_jardin = sc.nextInt(p.getPV());

    switch(choix_jardin){
         case 1 :
         String filePath1 = "./textes/dial_jardin/dial_aventure1.txt";
         System.out.println(Read.readLineByLine(filePath1));
         choix2(p.getPV());
         break;
         case 2 :
         String filePath2 = "./textes/dial_jardin/dial_rentrer.txt";
         System.out.println(Read.readLineByLine(filePath2));
         (new DialogueCafeteriat()).choixCornelien(p.getPV());
         break;
         default :
         System.out.println("Choix non reconnu");
         choix1(p.getPV());
         break;
  }
}

  public void choix2(int PV){
    p.setPV(PV);
    String choix2 = "./textes/dial_jardin/dial_choix2_ask.txt";
    System.out.println(Read.readLineByLine(choix2));
    Scanner sc = new Scanner(System.in);
    int choix_jardin = sc.nextInt();

    switch(choix_jardin){
         case 1 :
         String filePath1 = "./textes/dial_jardin/dial_aventure2.txt";
         System.out.println(Read.readLineByLine(filePath1));
         choix3(p.getPV());
         break;
         case 2 :
         String filePath2 = "./textes/dial_jardin/dial_rentrer.txt";
         System.out.println(Read.readLineByLine(filePath2));
         (new DialogueCafeteriat()).choixCornelien(p.getPV());
         break;
         default:
         System.out.println("Choix non reconnu");
         choix2(p.getPV());
         break;
  }
}

  public void choix3(int PV){
    p.setPV(PV);
    String choix3 = "./textes/dial_jardin/dial_choix3_ask.txt";
    System.out.println(Read.readLineByLine(choix3));
    Scanner sc = new Scanner(System.in);
    int choix_jardin = sc.nextInt();

    switch(choix_jardin){
         case 1 :
         String filePath1 = "./textes/dial_jardin/dial_aventure3.txt";
         System.out.println(Read.readLineByLine(filePath1));
         baisserPV();
         if (p.getPV() <= 0) {
             (new Mort()).GameOver();
           }
         (new DialogueCafeteriat()).choixCornelien(p.getPV());
         break;
         case 2 :
         String filePath2 = "./textes/dial_jardin/dial_rentrer.txt";
         System.out.println(Read.readLineByLine(filePath2));
         (new DialogueCafeteriat()).choixCornelien(p.getPV());
         break;
         default :
         System.out.println("Choix non reconnu");
         choix3(p.getPV());
         break;
       }
  }

      public void baisserPV(){
        Random random = new Random();
        int nb = random.nextInt(6);
        p.setPV(p.getPV()-nb);
        System.out.println("Vous avez pris "+nb+" point(s) de degats, il vous reste " + p.getPV() + "/" + p.getMaxPV());
      }
}
