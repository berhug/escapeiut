# README #


### Les membres du projet EscapeIUT :###

* Hugo Bernard : 			hugo.bernard5@etu.univ-lorraine.fr
* Eugenie Karadjoff : 			eugenie.karadjoff@gmail.com
* Anne-Sophie Joscht : 			asjoscht@gmail.com
* Laura Sofia Masmela Castano : 	lsofmascastanio@gmail.com
* Florent Grandjean : 			florent.grandjean6@etu.univ-lorraine.fr

### Participation pre-depot actuel: ###

* Anne-Sophie Joscht:
*	Correction du scenario
*	Division en classes separees de Perso / Dialogues / PointsVie et division de methodes
*	Apport des classes concernant la fin du scenario: la fuite finale
*	Ajout de DialogueSecretariat

* Eugenie Karadjoff:
*	Participation au scénario
*	Organisation et transformation des textes de toutes les classes Dialogues
*	en input .txt, appelés au moyen de la classe Read

* Laura Sofia Masmela Castano
*	Transcription du scénario à fichier XML et classe de sauvegarde.

* Hugo Bernard :
*	Création du scénario + cahier des charges
*	Construction des fichiers de bases tel que DialogueTenib, dialogueSalle105,...
*	Construction des fichiers d'apport tel que Combat, CombatCrous, CombatTENODAL, Plan,...
*	Maintenace de la structure du projet. 
*	Correction des erreurs et amélioration du code dans la quasi totalité des classes Java.
*	Correction de certains fichiers text.

* Florent Grandjean :
*   Transformation des classes Perso et PointsVie en un Singleton 
*	Amélioration de l'inventaire

### Pour le déploiement du jeu : ###

* 1 ère étape : effectuer un git clone
* 2 ème étape : dans le terminal, retrouver ce répertoire puis executer la commande javac *.java
* 3 ème étape : lancer le jeu, executer dans le terminal la commande java Game

### Attention : ###

* Ne tentez pas de déployer le XML, ce dossier est actuellement en maintenace, il sera corrigé dans un prochain DLC.