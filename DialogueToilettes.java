import java.util.Scanner;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import javax.swing.JFrame;
import java.awt.Dimension;

public class DialogueToilettes extends Dialogue{

  public void Intro_Toilettes(int PV) {
    p.setPV(PV);
     String intro = "./textes/dial_wc/dial_wc_intro.txt";
     System.out.println(Read.readLineByLine(intro));
     choixCornelien(p.getPV());
  }

  public void choixCornelien(int PV)
  {
    p.setPV(PV);
     String choixcornel = "./textes/dial_wc/dial_wc_ask.txt";
     System.out.println(Read.readLineByLine(choixcornel));
     Scanner sc = new Scanner(System.in);
     int choix_wc = sc.nextInt();

     switch(choix_wc){
          case 1 :
            String filePath1 = "./textes/dial_wc/dial_wc_rep1.txt";
            System.out.println(Read.readLineByLine(filePath1));
            (new DialogueCouloir()).choisirSalle(p.getPV());
            break;
          case 2 :
            String filePath2 = "./textes/dial_wc/dial_wc_rep2.txt";
            System.out.println(Read.readLineByLine(filePath2));
            (new DialogueCouloir()).choisirSalle(p.getPV());
            break;
          case 3 :
            String filePath3 = "./textes/dial_wc/dial_wc_rep3.txt";
            System.out.println(Read.readLineByLine(filePath3));
            (new DialogueCouloir()).choisirSalle(p.getPV());
            break;
          case 4 :
            String filePath4 = "./textes/dial_wc/dial_wc_rep4.txt";
            System.out.println(Read.readLineByLine(filePath4));
            (new DialogueCouloir()).choisirSalle(p.getPV());
            break;
          case 5 :
            String filePath5 = "./textes/dial_wc/dial_wc_rep5.txt";
            System.out.println(Read.readLineByLine(filePath5));
            (new Inventaire()).Liste_inventaire(1);
            (new DialogueCouloir()).choisirSalle(p.getPV());
            break;
          case 6 :
            String filePath6 = "./textes/dial_wc/dial_wc_rep6.txt";
            System.out.println(Read.readLineByLine(filePath6));
            (new DialogueCouloir()).choisirSalle(p.getPV());
            break;
          case 7 :
            int salle = 14;
            JFrame fenetre = new JFrame ("Plan de l'IUT :");
            Plan dessin = new Plan(salle);
            dessin.setPreferredSize(new Dimension (1200,480));
            fenetre.setContentPane(dessin);
            fenetre.pack();
            fenetre.setVisible(true);
            choixCornelien(p.getPV());
            break;
          default :
            System.out.println("Choix non reconnu");
            choixCornelien(p.getPV());
            break;
     }
  }
}
