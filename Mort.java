import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;


/**
 * @author asjos, ek
 */
public class Mort extends Dialogue
{

  public void GameOver()
  {
    String gameOver = "./textes/game_over.txt";
    System.out.println(Read.readLineByLine(gameOver));
    System.exit(0);
  }

  public void mourirTomber()
  {
      String dead = "./textes/dial_bureauIUT/mort.txt";
      System.out.println(Read.readLineByLine(dead));
      GameOver();
  }


}
