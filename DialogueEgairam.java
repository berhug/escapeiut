import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.util.Random;
import javax.swing.JFrame;
import java.awt.Dimension;

public class DialogueEgairam extends Dialogue {



  public void Intro_Egairam(String s, int PV){
    p.setPV(PV);
    String intro = "./textes/dialeg/dialeg_intro.txt";
    System.out.println(Read.readLineByLine(intro));

    if(s == "cafe")
    {
            ChoixCafe(p.getPV());
    }
    else if(s == "nourriture")
    {
            ChoixNourriture(p.getPV());
    }
    else if(s == "boisson")
    {
            ChoixBoisson(p.getPV());
    }
    else
    {
            Retard(p.getPV());
    }
  }

  public void ChoixCafe(int PV)
  {
    p.setPV(PV);
    String furax = "./textes/dialeg/cafe_ask.txt";
    System.out.println(Read.readLineByLine(furax));
    Scanner sc = new Scanner(System.in);
    int choixfurax = sc.nextInt();

    String furax2 = "./textes/dialeg/cafe_all.txt";
    System.out.println(Read.readLineByLine(furax2));
    baisserPV(p.getPV());
    if (p.getPV() > 0) {
        (new DialogueCouloir()).choisirSalle(p.getPV());
      }
  }

  public void ChoixNourriture(int PV)
  {
    p.setPV(PV);
    String furious = "./textes/dialeg/cafe_ask.txt";
    System.out.println(Read.readLineByLine(furious));
    Scanner sc = new Scanner(System.in);
    int choixfurious = sc.nextInt();

    String furious2 = "./textes/dialeg/nourri_all.txt";
    System.out.println(Read.readLineByLine(furious2));
    baisserPV(p.getPV());
    if (p.getPV() > 0) {
        (new DialogueCouloir()).choisirSalle(p.getPV());
      }
  }

  public void ChoixBoisson(int PV)
  {
    p.setPV(PV);
    String madness = "./textes/dialeg/cafe_ask.txt";
    System.out.println(Read.readLineByLine(madness));
    Scanner sc = new Scanner(System.in);
    int choixmad = sc.nextInt();

    String furious2 = "./textes/dialeg/boisson_all.txt";
    System.out.println(Read.readLineByLine(furious2));
    baisserPV(p.getPV());
    if (p.getPV() > 0) {
        (new DialogueCouloir()).choisirSalle(p.getPV());
      }
  }

  public void Retard(int PV)
  {
    p.setPV(PV);
    String furax = "./textes/dialeg/dial_notime.txt";
    System.out.println(Read.readLineByLine(furax));
    Scanner sc = new Scanner(System.in);
    int retard = sc.nextInt();

    String furious = "./textes/dialeg/dial_furax.txt";
    System.out.println(Read.readLineByLine(furious));
    baisserPV(p.getPV());
    if (p.getPV() > 0) {
        (new DialogueCouloir()).choisirSalle(p.getPV());
      }
  }

  public void baisserPV(int PV)
  {
      Random random = new Random();
      int degats = random.nextInt(6);
      p.setPV(p.getPV()-degats);
      System.out.println("Vous avez pris " +degats+" point(s) de degats, il vous reste " + p.getPV() + "/" + p.getMaxPV() + " PV.");
      if(p.getPV() < 0) {
        (new Mort()).GameOver();
      }
  }
}
